# Kinetic

`kinetic` is a Python module collecting functions useful for simulation of chemical and enzymatic kinetics.

# Functions
The main functionality of the module is covered by functions:

`reaction(rxn_str)` function, which takes one argument:
* `rxn_str` text represenation of the reaction. The reaction specification syntax is simple, with two reaction types available: **reversible** by using `<=>` symbol  or **irreversible** `->`. The reagents are separated by `+` sign and spaces. The rate constants are named at the end after the `;` symbol. This function return a dictionary with all the reaction details, for example:
```
>>> reaction('E + S <=> ES ; k1on k1off')
{'type': 'reversible',
 'substrates': {'E': 1, 'S': 1},
 'products': {'ES': 1},
 'rateconst': {'forward': 'k1on', 'backward': 'k1off'}}

>>> reaction('2M -> M2 ; k')
{'type': 'irreversible',
 'substrates': {'M': 2},
 'products': {'M2': 1},
 'rateconst': {'forward': 'k', 'backward': None}}
```

`concentration(rxns, c0, rates, ts)` function takes four arguments:
* `rxns` *(type: list)* a list of reactions, created by the **reaction** function
* `c0` *(type: dict)* a dictionary of initial conditions. Only species with non-zero concentrations should be included
* `rates` *(type: dict)* a dictionary holding the values of all defined kinetic rate constants
* `ts` *(type: np.array)* an array with discrete time points, for which the transient concentration will be calculated

The function returns the species identifiers and the transient concentrations array.

Simple example:
```
rxns = [kn.reaction('E + S <=> ES ; k1on k1off'),
        kn.reaction('ES -> E + P  ; k2')]

c0 = {'E': 1.0e-3, 'S': 1.0}
kin = {'k1on': 1.0e3, 'k1off': 200.0, 'k2': 250.0}
ts = np.linspace(0.0, 20, 21)

spcs, ct = kn.concentration(rxns, c0, kin, ts)
```

The species identifiers `spcs` are the column numbers in the transient concentrations matrix `ct`:
```
>>> spcs
{'E': 0, 'S': 1, 'ES': 2, 'P': 3}
>>> ct.shape                                                                                                                   
(21, 4)
```
 Thus, to access the concentrations of P:
```
>>> ct[:, spcs['P']]                                                                                                           
array([0.        , 0.16735927, 0.32370785, 0.46672003, 0.5939576 ,
       0.70304374, 0.79221334, 0.86102881, 0.91088389, 0.94483588,
       0.96675289, 0.98032976, 0.98850121, 0.99332778, 0.99614564,
       0.99777928, 0.9987225 , 0.99926576, 0.99957822, 0.99975777,
       0.99986091])
```
