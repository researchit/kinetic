import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import kinetic as kn

#%%
rxns = [kn.reaction('A -> B ; k1'),
        kn.reaction('B -> C ; k2')]

c0 = {'A': 10.0}
kin = {'k1': 0.5, 'k2': 0.2}
ts = np.linspace(0.0, 10, 21)

#%%
spcs, ct = kn.concentration(rxns, c0, kin, ts)

#%% randomize
np.random.seed(42)

sigma = 0.05
ct += sigma*np.random.randn(*ct.shape)

#%%
df = pd.DataFrame({'time': ts,
                   'A': ct[:, spcs['A']],
                   'C': ct[:, spcs['C']]})

df.to_csv('abc.csv', index=None)

#%%
print(spcs)
for spc in spcs:
    plt.plot(ts, ct[:, spcs[spc]], '.-', label=spc)

#%%

plt.grid()
plt.legend()
plt.show()

# for i in range(len(ts)):
#    print('%8.3f %12.6e' % (ts[i], ct[i, spcs['S']]))