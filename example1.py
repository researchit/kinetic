import matplotlib.pyplot as plt
import numpy as np

import kinetic as kn

# E + S <=> ES
# ES -> E + P

rxns = [kn.reaction('E + S <=> ES ; k1on k1off'),
        kn.reaction('ES -> E + P  ; k2')]

c0 = {'E': 1.0e-3, 'S': 1.0}
kin = {'k1on': 1.0e3, 'k1off': 200.0, 'k2': 250.0}
ts = np.linspace(0.0, 20, 21)

spcs, ct = kn.concentration(rxns, c0, kin, ts)

print(spcs)
for spc in spcs:
    plt.plot(ts, ct[:, spcs[spc]], '.-', label=spc)

plt.grid()
plt.legend()
plt.show()

for i in range(len(ts)):
    print('%8.3f %12.6e' % (ts[i], ct[i, spcs['S']]))
