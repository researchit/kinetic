# -*- coding: utf-8 -*-

import re
import numpy as np

import kinetic

#%%

def concentrations_model(c0, rates, t, hasAMPPNP=False):
    """
    Calculate transient concentrations
    """
    roots = [  '_HD',  '_HpD',  '_HDp',  '_HpDp']

    roots_locked = [s.replace('_', '=') for s in roots]

    ligands = ['ATP', 'ADP']

    if hasAMPPNP:
        ligands.append('AMPPNP')

    # ligand equilibria
    ligand_eq_rxns_s = []

    for root in roots + roots_locked:
        for l in ligands:
            ligand_eq_rxns_s.append('%s.%s <=> %s.free + %s    ; kon koff'%(root, l, root, l))

    # active <=> locked equilibria
    active_locked_eq_rxns_s = []

    for active, locked in zip(roots, roots_locked):
        #print(active, ' ', locked)
        for l in ligands + ['free']:
            active_locked_eq_rxns_s.append('%s.%s <=> %s.%s    ; k01 k01r'%(active, l, locked, l))

    #
    eq_rxns= [kinetic.reaction(rxn_s) for rxn_s in ligand_eq_rxns_s ]
    active_species = {k:v for k,v in kinetic.get_species(eq_rxns).items() if k.count('_')>0 }
    eq_species = kinetic.get_species(eq_rxns)

    # add reactions with k1
    k1_rxns_s = []

    k1_rxns_s.append('_HD.ATP <=> _HpD.ADP   ; k1 k1r')
    k1_rxns_s.append('_HDp.ATP <=> _HpDp.ADP   ; k1 k1r')

    # non-specific Hp dephosphorylation
    k1non_rxns_s = []

    for spc, spc_id in eq_species.items():
        idxs = [match.start() for match in re.finditer('Hp', spc)]
        for idx in idxs:
            prefix = spc[:idx]
            suffix = spc[idx+2:]
            k1non_rxns_s.append('%s -> %sH%s + P    ; k1non'%(spc, prefix, suffix))

    # add reactions with k2
    k2_rxns_s = []

    for spc, spc_id in active_species.items():
        idx = spc.find('_HpD.')
        if idx >= 0:
            prefix = spc[:idx]
            suffix = spc[idx+5:]
            k2_rxns_s.append('%s <=> %s_HDp.%s    ; k2 k2r'%(spc, prefix, suffix))

    # add reactions with k3
    k3_rxns_s = []

    # k3_rxns_s.append('_HDp.ADP -> _HD.ADP + P   ; k3adp')

    for spc, spc_id in eq_species.items():
        idxs = [match.start() for match in re.finditer('_HDp.', spc)]
        for idx in idxs:
            suffix = spc[idx+5:]
            # k3_rxns_s.append('%s -> _HD.%s + P    ; k3any'%(spc, suffix))
            k3_rxns_s.append('%s -> _HD.%s + P    ; k3'%(spc, suffix))

#    for spc, spc_id in active_species.items():
#        idx = spc.find('_HDp.ADP')
#        if idx > 0:
#            prefix = spc[:idx]
#            k3_rxns_s.append('%s -> %s_HD.ADP + P ; k3'%(spc, prefix))
#
#        if spc.find('ADP.DpH_') >= 0:
#            suffix = spc[8:]
#            k3_rxns_s.append('%s -> ADP.DH_%s + P ; k3'%(spc, suffix))

    # non-specific Dp dephosphorylation
    k4_rxns_s = []

    for spc, spc_id in eq_species.items():
        idxs = [match.start() for match in re.finditer('Dp', spc)]
        for idx in idxs:
            prefix = spc[:idx]
            suffix = spc[idx+2:]
            k4_rxns_s.append('%s -> %sD%s + P    ; k4'%(spc, prefix, suffix))

    # ATP hydrolysis
    unspecific = ['ATP -> ADP + P    ; k02']

    #
    rxns = [kinetic.reaction(rxn_s) for rxn_s in ligand_eq_rxns_s + active_locked_eq_rxns_s + k1_rxns_s + k1non_rxns_s + k2_rxns_s + k3_rxns_s + k4_rxns_s + unspecific]

    rxns_dict = {'ligand_eq': ligand_eq_rxns_s,
                 'active_locked_eq': active_locked_eq_rxns_s,
                 'k1': k1_rxns_s,
                 'k1non': k1non_rxns_s,
                 'k2': k2_rxns_s,
                 'k3': k3_rxns_s,
                 'k4': k4_rxns_s,
                 'unspecific': unspecific}

    #
    species, ct = kinetic.concentration(rxns, c0, rates, t)

    # calculate transient total ADP concentration
    ctADP_total = np.zeros(ct.shape[0])
    ctATP_total = np.zeros(ct.shape[0])
    ctBoundP_total = np.zeros(ct.shape[0])

    for spc, spc_id in species.items():
        nADP = spc.count('ADP')
        if nADP > 0:
            ctADP_total += nADP * ct[:, spc_id]

        nATP = spc.count('ATP')
        if nATP > 0:
            ctATP_total += nATP * ct[:, spc_id]

        n_p = spc.count('p')
        if n_p > 0:
            ctBoundP_total += n_p * ct[:, spc_id]

    return species, rxns_dict, ct, ctADP_total, ctATP_total, ctBoundP_total


def equilibrate_model(c0, rates, dt=1.0, npoints=10, eps=1e-3, hasAMPPNP=False):
    """
    Equilibrate ligands binding and locked<->active states
    """

    roots = [  '_HD',  '_HpD',  '_HDp',  '_HpDp']

    roots_locked = [s.replace('_', '=') for s in roots]

    ligands = ['ATP', 'ADP']

    if hasAMPPNP:
        ligands.append('AMPPNP')

    # ligand equilibria
    ligand_eq_rxns_s = []

    for root in roots + roots_locked:
        for l in ligands:
            ligand_eq_rxns_s.append('%s.%s <=> %s.free + %s    ; kon koff'%(root, l, root, l))

    # active <=> locked equilibria
    active_locked_eq_rxns_s = []

    for active, locked in zip(roots, roots_locked):
        #print(active, ' ', locked)
        for l in ligands + ['free']:
            active_locked_eq_rxns_s.append('%s.%s <=> %s.%s    ; k01 k01r'%(active, l, locked, l))

    #
    rxns = [kinetic.reaction(rxn_s) for rxn_s in ligand_eq_rxns_s + active_locked_eq_rxns_s]

    #
    t = np.linspace(0, dt, npoints)

    species, ct = kinetic.concentration(rxns, c0, rates, t)

    i = 1
    chi2 = np.sqrt(np.sum((ct[-1,:] - ct[-2,:])**2))
    c1 = kinetic.make_conc_dict(species, ct[-1, :])

    while chi2 > eps:
        print('iteration %d:   %f'%(i, chi2))
        c1 = kinetic.make_conc_dict(species, ct[-1, :])
        species, ct = kinetic.concentration(rxns, c1, rates, t)
        chi2 = np.sqrt(np.sum((ct[-1,:] - ct[-2,:])**2))
        i+=1

    return species, c1
