#!/struct/soft/osx/anaconda3/bin/python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from shka_model_monomer import concentrations_model, equilibrate_model

#%%
df_fplc  = pd.read_csv('fig7/f7minus_fplc.csv')
df_fplc2 = pd.read_csv('fig7/f7plus_fplc.csv')

df_auto  = pd.read_csv('fig7/f7minus_auto.csv')
df_auto2 = pd.read_csv('fig7/f7plus_auto.csv')

#%% normalize auto
df_auto.Signal /= 1e8
df_auto2.Signal /= 1e8

#%% 7a
plt.figure(figsize=(10,5))
plt.subplot(1,2,1)
plt.plot(df_auto.Time, df_auto.Signal, 'bx')
plt.plot(df_auto2.Time, df_auto2.Signal, 'gx')
#plt.xlim((0,1000))
plt.ylim((0,3))

plt.subplot(1,2,2)
# 7c
plt.plot(df_fplc['time [sec]'], df_fplc['area(ADP)'], 'bx')
plt.plot(df_fplc2['time [sec]'], df_fplc2['area(ADP)'], 'rx')
#plt.xlim((0,1000))
plt.ylim((0,3))

#%% User input:
K1 = 0.33
K2 = 2.0
K01 = 50.0

# initial conditions
c0 = {'_HD.free': 5.0, 'ATP': 200.0}

# rate constants
k01  = 100.0       #   _HD -> DH=HD
k01r = k01/K01     #   =HD -> DH_HD
k02  = 0.0         #   ATP -> ADP + P

k1    = 0.01   #   H.ATP -> Hp.ADP
k1r   = k1/K1  #  Hp.ADP -> H.ATP
k1non = 0.0    #      Hp -> H + P
k2    = 0.0    # HpD.ATP -> HDp.ATP
k2r   = k2/K2
k3    = 0.0    # HDp.ADP -> HD.ADP + P
k4    = 0.0    #      Dp -> D + P

#%% equilibrate system

Kd = 50 # uM
kon = 100.0 # uM-1s-1 10^8 M-1s-1  diffusion limit
koff = Kd*kon

rates = { 'kon': kon,
         'koff': koff,
          'k01': k01,
         'k01r': k01r}

species, c1 = equilibrate_model(c0, rates)

#%%
rates = { 'kon': kon,
         'koff': koff,
          'k01': k01,
         'k01r': k01r,
          'k02': k02,
           'k1': k1,
          'k1r': k1r,
        'k1non': k1non,
           'k2': k2,
          'k2r': k2r,
           'k3': k3,
           'k4': k4}

#%% error function
def errFunction(p):
    global K1
    global rates, df_auto, df_fplc, c1
    global species, rxns_d1, ct1, ct1_adp, ct1_atp, ct1_boundp
    global ct2, ct2_adp, ct2_atp, ct2_boundp
    global t1, t2

    k1, sc_auto, sc_fplc = p
    print('Params: ', p, end='')

    rates['k1'] = k1
    rates['k1r'] = k1 / K1

    t1 = np.insert(df_auto.Time.values, 0, 0)
    species, rxns_d1, ct1, ct1_adp, ct1_atp, ct1_boundp = concentrations_model(c1, rates, t1)

    t2 = np.insert(df_fplc['time [sec]'].values, 0, 0)
    species, rxns_d1, ct2, ct2_adp, ct2_atp, ct2_boundp = concentrations_model(c1, rates, t2)

    d1 = df_auto.Signal.values - sc_auto*ct1_boundp[1:]
    d2 = df_fplc['area(ADP)'].values - sc_fplc*ct2_adp[1:]

    d = np.append(d1, d2)

    print('  chi^2: %8.3f'%np.sum(d**2))

    return d

#%%
p0 = [0.3, 0.3, 0.4] # k1, sc_auto, sc_fplc

d = errFunction(p0)

#%% 7a
_, sc_auto, sc_fplc = p0

plt.figure(figsize=(10,5))
plt.subplot(1,2,1)
# exp
plt.plot(df_auto.Time, df_auto.Signal, 'bx', label='-cdG')
plt.plot(df_auto2.Time, df_auto2.Signal, 'gx', label='+cdG')
# calc
plt.plot(t1, sc_auto*ct1_boundp, '.-', label='bound P')
plt.legend()

#plt.xlim((0,1000))
plt.ylim((0,3))

# 7c
plt.subplot(1,2,2)
plt.plot(df_fplc['time [sec]'], df_fplc['area(ADP)'], 'bx', label='-cdG')
plt.plot(df_fplc2['time [sec]'], df_fplc2['area(ADP)'], 'rx', label='+cdG')
# calc
plt.plot(t2, sc_fplc*ct2_adp, '.-', label='ADP')
#plt.xlim((0,1000))
plt.ylim((0,3))

plt.legend()

#%%
from scipy.optimize import least_squares

#%%

res = least_squares(errFunction, p0, bounds=(0.0, np.inf))

#%% optimized parameter values
p1 = res.x

#%%
d = errFunction(p1)

#%% 7a
_, sc_auto, sc_fplc = p1

plt.figure(figsize=(10,5))
plt.subplot(1,2,1)
# exp
plt.plot(df_auto.Time, df_auto.Signal, 'bx', label='-cdG')
plt.plot(df_auto2.Time, df_auto2.Signal, 'gx', label='+cdG')
# calc
plt.plot(t1, sc_auto*ct1_boundp, '.-', label='bound P')
plt.legend()

#plt.xlim((0,1000))
plt.ylim((0,3))

# 7c
plt.subplot(1,2,2)
plt.plot(df_fplc['time [sec]'], df_fplc['area(ADP)'], 'bx', label='-cdG')
plt.plot(df_fplc2['time [sec]'], df_fplc2['area(ADP)'], 'rx', label='+cdG')
# calc
plt.plot(t2, sc_fplc*ct2_adp, '.-', label='ADP')
#plt.xlim((0,1000))
plt.ylim((0,3))

plt.legend()
