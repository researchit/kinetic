import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from scipy.optimize import least_squares

import kinetic as kn

#%%
df = pd.read_csv('abc.csv')

#%%
plt.plot(df.time, df.A, '.-', label='A')
plt.plot(df.time, df.C, '.-', label='C')

plt.grid()
plt.legend()
plt.show()

#%%
def residuals(p):
    global df
    
    k1, k2 = p
    
    rxns = [kn.reaction('A -> B ; k1'),
            kn.reaction('B -> C ; k2')]    
    
    c0 = {'A': 10.0}
    kin = {'k1': k1, 'k2': k2}
    ts = np.array(df.time)
    
    spcs, ct = kn.concentration(rxns, c0, kin, ts)
    
    rA = df.A - ct[:, spcs['A']]
    rC = df.C - ct[:, spcs['C']]
    rsds = np.concatenate([rA, rC])
    
    print(p, f'chi2: {np.sum(rsds**2)}')
    
    return rsds

#%%
p0 = [1.0, 1.0] # k1, k2

# print(residuals(p0))

#%%
res = least_squares(residuals, p0)

p_opt = res.x

